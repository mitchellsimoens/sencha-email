module.exports = {
    get Email () {
        return require('./Email');
    },

    get Manager () {
        return require('./Manager');
    },

    get Provider () {
        return require('./Provider');
    }
};
